# The MIT License (MIT)
#
# Copyright (c) 2023 Daniel DeBruno
# Copyright (c) 2014-2023 Omar Cornut
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Cross Platform Makefile
# Compatible with MSYS2/MINGW, Ubuntu 14.04.1 and Mac OS X
#
# You will need SDL2 (http://www.libsdl.org):
# Linux:
#   apt-get install libsdl2-dev
# Mac OS X:
#   brew install sdl2
# MSYS2:
#   pacman -S mingw-w64-i686-SDL2
#

#CXX = g++
#CXX = clang++

SRCDIR = src
OBJDIR = obj
BINDIR = bin
IMGUI_DIR = ../imgui
IMGUI_OBJDIR = $(OBJDIR)/imgui

TARGET  = reney
SOURCES = $(wildcard $(SRCDIR)/*.cpp)
IMGUI_SOURCES = $(IMGUI_DIR)/imgui.cpp $(IMGUI_DIR)/imgui_demo.cpp $(IMGUI_DIR)/imgui_draw.cpp $(IMGUI_DIR)/imgui_tables.cpp $(IMGUI_DIR)/imgui_widgets.cpp
IMGUI_SOURCES += $(IMGUI_DIR)/backends/imgui_impl_sdl2.cpp $(IMGUI_DIR)/backends/imgui_impl_opengl3.cpp
OBJECTS := $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(SOURCES:.cpp=.o))
IMGUI_OBJECTS := $(patsubst $(IMGUI_DIR)/%.cpp,$(IMGUI_OBJDIR)/%.o,$(IMGUI_SOURCES))


UNAME_S := $(shell uname -s)

CXXFLAGS = -std=c++11 -I$(IMGUI_DIR) -I$(IMGUI_DIR)/backends
CXXFLAGS += -g -Wall -Wformat
LIBS =

##---------------------------------------------------------------------
## OPENGL ES
##---------------------------------------------------------------------

## This assumes a GL ES library available in the system, e.g. libGLESv2.so
# CXXFLAGS += -DIMGUI_IMPL_OPENGL_ES2
# LINUX_GL_LIBS = -lGLESv2
## If you're on a Raspberry Pi and want to use the legacy drivers,
## use the following instead:
# LINUX_GL_LIBS = -L/opt/vc/lib -lbrcmGLESv2

##---------------------------------------------------------------------
## BUILD FLAGS PER PLATFORM
##---------------------------------------------------------------------

ifeq ($(UNAME_S), Linux) #LINUX
	ECHO_MESSAGE = "Linux"
	LIBS += -lGL -ldl `sdl2-config --libs` `pkg-config --libs guile-2.2`

	CXXFLAGS += `sdl2-config --cflags` `pkg-config --cflags guile-2.2`
	CFLAGS = $(CXXFLAGS)
endif

ifeq ($(UNAME_S), Darwin) #APPLE
	ECHO_MESSAGE = "Mac OS X"
	LIBS += -framework OpenGL -framework Cocoa -framework IOKit -framework CoreVideo `sdl2-config --libs`
	LIBS += -L/usr/local/lib -L/opt/local/lib

	CXXFLAGS += `sdl2-config --cflags`
	CXXFLAGS += -I/usr/local/include -I/opt/local/include
	CFLAGS = $(CXXFLAGS)
endif

ifeq ($(OS), Windows_NT)
    ECHO_MESSAGE = "MinGW"
    LIBS += -lgdi32 -lopengl32 -limm32 `pkg-config --static --libs sdl2`

    CXXFLAGS += `pkg-config --cflags sdl2`
    CFLAGS = $(CXXFLAGS)
endif

##---------------------------------------------------------------------
## BUILD RULES
##---------------------------------------------------------------------

all: $(BINDIR)/$(TARGET)
	@echo Build complete for $(ECHO_MESSAGE)

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	@mkdir -p $(OBJDIR)
	$(CXX) $(CXXFLAGS) -c $< -o $@
	@echo "Compiled "$<" successfully!"

$(IMGUI_OBJDIR)/%.o : $(IMGUI_DIR)/%.cpp
	@mkdir -p $(IMGUI_OBJDIR)
	$(CXX) $(CXXFLAGS) -c $< -o $@
	@echo "Compiled "$<" to "$@" successfully!"

$(IMGUI_OBJDIR)/backends/%.o : $(IMGUI_DIR)/backends/%.cpp
	@mkdir -p $(IMGUI_OBJDIR)/backends
	$(CXX) $(CXXFLAGS) -c $< -o $@
	@echo "Compiled "$<" to "$@" successfully!"

$(BINDIR)/$(TARGET): $(OBJECTS) $(IMGUI_OBJECTS)
	@mkdir -p $(BINDIR)
	$(CXX) $(OBJECTS) $(IMGUI_OBJECTS) $(CXXFLAGS) $(LIBS) -o $@
	@echo "Linking complete!"

.PHONY: clean
clean:
	@rm -rf $(OBJECTS) $(IMGUI_OBJECTS)
	@echo "Cleanup complete!"

.PHONY: remove
remove: clean
	@rm -rf $(BINDIR)/$(TARGET)
	@echo "Executable removed!"
