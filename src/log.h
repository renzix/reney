#ifndef __LOG_H__
#define __LOG_H__

#include <iostream>
#include <cstdarg>

namespace rlog {

    enum LogVisibility {
      DEBUG = 0,
      INFO = 1,
      WARNING = 2,
      ERROR = 3,
      NONE = 4
    };

    static LogVisibility LOG_VISIBILITY = DEBUG;

    template<typename T, typename... Args>
    inline void print(LogVisibility vis, T value, Args... args)
    {
        if (vis<=LOG_VISIBILITY)
            return;
        std::cout << value;

        print(vis, args...);
    }

    template<typename T>
    inline void print(LogVisibility vis, T value)
    {
        if (vis<=LOG_VISIBILITY)
            return;

        std::cout << value << std::endl;
    }

}

#endif // __LOG_H__
